import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import ViewList from '@material-ui/icons/ViewList'
import MoreHoriz from '@material-ui/icons/MoreHoriz'
import FilterList from '@material-ui/icons/FilterList'
import TextField from '@material-ui/core/TextField'
import Task from "./task"
import Collapse from '@material-ui/core/Collapse'




const styles = theme => ({
    container: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    },
})

class TaskList extends Component {
    constructor() {
        super()
        this.state = {
            isOpen: false
        }
    }
    handleOpen = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    getTask() {
        if(this.state.isOpen)
            return <Task />
        else
            return null
    }
    render() {
        const { classes } = this.props;
        return(
            <div>
                <div className='task-list'>
                    <div className={classes.container}>
                        <div  className="task-list-text">    
                            <Typography variant="body2">
                                Открытые задачи
                            </Typography>
                        </div>
                        <IconButton className={classes.button} aria-label="Add" onClick={this.handleOpen}>
                            <ViewList />
                        </IconButton>
                        <TextField
                            id="with-placeholder"
                            label="Cумма по Оцен..."
                            placeholder="347"
                            margin="none"
                            padding="none"
                            style={{width: 110}}
                            InputProps={{
                                style: {fontSize: "0.8em"}
                                }}
                              InputLabelProps={{
                                  style: {fontSize: "0.8em"}
                                }}
                        />
                    </div> 
                    <div>
                        <IconButton className={classes.button} aria-label="MoreHoriz">
                            <FilterList />
                        </IconButton>
                        <IconButton className={classes.button} aria-label="MoreHoriz">
                            <MoreHoriz />
                        </IconButton>
                    </div>
                </div>
                <div>
                    <Collapse in={this.state.isOpen} timeout="auto" unmountOnExit>  
                        {this.getTask()}
                    </Collapse>
                </div>
            </div>
        )
    }
}

TaskList.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(TaskList);