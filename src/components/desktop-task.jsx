import React, {Component} from 'react'
import IconButton from '@material-ui/core/IconButton'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Card from '@material-ui/core/Card'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import Chip from '@material-ui/core/Chip'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Checkbox from '@material-ui/core/Checkbox'


  


class DesktopTask extends Component {
    constructor() {
        super() 
        this.state = {
            chipData: [
                { key: 0, label: 'Angular' },
                { key: 1, label: 'jQuery' },
                { key: 2, label: 'Polymer' },
                { key: 3, label: 'React' },
                { key: 4, label: 'Vue.js' },
            ],
            currencies: [
                {
                  value: 'USD',
                  label: 'to do',
                },
                {
                  value: 'EUR',
                  label: 'done',
                },
                {
                  value: 'BTC',
                  label:  <Checkbox color="primary" checked="true" label="" />,
                },
              ],
            name: "Victor",
            isOpen: false,
            listTask: [],
            currency: 'to do',
            dateStart: "2017-05-24",
            dateStop: "2017-05-24",
            score: "127"

        }
    }
    handleDelete = data => () => {
        this.setState(state => {
          const chipData = [...state.chipData];
          const chipToDelete = chipData.indexOf(data);
          chipData.splice(chipToDelete, 1);
          return { chipData };
        });
    }
    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    getBody() {
        if (this.state.isOpen) return this.props.task
        else return null
    }
    getMore() {
        if (!this.props.fourth) return  <IconButton  aria-label="ExpandMore" onClick={this.handleClick}>
                                            {this.state.isOpen ? <ExpandMore  /> : <ChevronRight />}
                                        </IconButton>
        else return null
    }
    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
      };
    
    render() {
        return (
            <div className="task-desktop">
                <Card>
                    <div className="task-desktop-header">    
                        <div className="task-desktop-title">
                            {this.getMore()}
                            <Typography variant="body2" color="primary" >
                                #{this.props.id}
                            </Typography>
                            <Avatar style={{width: 30,height: 30, margin: 10}} >IP</Avatar>
                            <TextField
                              id="title"
                              value={this.props.title}
                              multiline
                              onChange={this.handleChange('name')}
                              margin="normal"
                              InputProps={{
                                readOnly: true,
                                style: {fontSize: "0.8em" }
                              }}
                              InputLabelProps={{
                                style: {fontSize: "0.8em"}
                              }}
                            />
                        </div>
                        <div className="task-desktop-body">
                            <TextField
                                id="score"
                                label="Oценка"
                                defaultValue={this.state.score}
                                style={{margin: 10, width: (this.state.score.length * 8) }}
                                InputProps={{
                                    readOnly: true,
                                    style: {fontSize: "0.7em" }
                                  }}
                                InputLabelProps={{
                                    style: {fontSize: "0.8em"}
                                  }}
                            />
                            <TextField
                                id="number"
                                label="Версия"
                                defaultValue="1"
                                type="number"
                                style={{minWidth: 30, width: 20, margin: 10}}
                                margin="dense"
                                InputProps={{
                                  style: {fontSize: "0.8em"}
                                  }}
                                InputLabelProps={{
                                    style: {fontSize: "0.8em"}
                                  }}
                            />
                            <TextField
                                  id="select-currency"
                                  select
                                  label="Статус"
                                  style={{margin: 10,width: 60}}
                                  value={this.state.currency}
                                  onChange={this.handleChange()}
                                  autoWidth
                                  SelectProps={{
                                    
                                  }}
                                  InputProps={{
                                    style: {fontSize: "0.7em"},
                                    }}
                                  InputLabelProps={{
                                      style: {fontSize: "0.8em"},
                                    }}
                                  margin="normal"
                                >
                                  {this.state.currencies.map(option => (
                                    <MenuItem key={option.value} value={option.value}>
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                            <TextField
                                id="date1"
                                label="Birthday"
                                type="date"
                                defaultValue={this.state.dateStart}
                                style={{margin: 10, width: (this.state.dateStart * 6)}}
                                InputLabelProps={{
                                    shrink: true,
                                    style: {fontSize: "0.8em"}
                                  }}
                                InputProps={{
                                  style: {fontSize: "0.8em"}
                                  }}
                                margin="dense"
                            />
                            <TextField
                                id="name"
                                label="Автор"
                                defaultValue={this.state.name}
                                margin="dense"
                                style={{margin: 10,minWidth: 30, maxWidth: "20%"}}
                                InputProps={{
                                  style: {fontSize: "0.8em", display: "block", width: (this.state.name.length * 6)}
                                  }}
                                InputLabelProps={{
                                    style: {fontSize: "0.8em"}
                                  }}
                            />
                            <TextField
                                id="date2"
                                label="Birthday"
                                type="date"
                                defaultValue={this.state.dateStop}
                                style={{margin: 10, width: (this.state.dateStop * 6)}}
                                InputLabelProps={{
                                    shrink: true,
                                    style: {fontSize: "0.8em"}
                                  }}
                                InputProps={{
                                  style: {fontSize: "0.8em"}
                                  }}
                                margin="dense"
                            />
                        </div>
                    </div>
                    <div className="task-desktop-footer">
                        <Typography variant="body2" style={{float: "left"}} >
                              Теги:
                        </Typography>
                        <div className="task-desktop-footer-chip">
                            {this.state.chipData.map(data => 
                                <Chip                     
                                    key={data.key}
                                    label={data.label}
                                    onDelete={this.handleDelete(data)}
                                    color="primary"
                                    style={{height: 25}}
                                />)
                            }
                        </div>
                    </div>
                </Card>    
                {this.getBody()}            
            </div>
        )
    }
}
export default DesktopTask