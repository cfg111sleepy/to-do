import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import AddCircleOutline from '@material-ui/icons/AddCircleOutline'
import SearchIcon from '@material-ui/icons/Search'
import IconButton from '@material-ui/core/IconButton'

const styles = theme => ({
    menu: {
        width: 200,
    },
    button: {
        margin: theme.spacing.unit,
      }
  });



class AddSearch extends Component {
    constructor() {
        super()

        this.state = {
            multiline: 'Controlled'
          };
    }
    handleChange = event => {
        
      };

    render() {
        const { classes } = this.props
        return (
            <div className='add-search'>
                <IconButton className={classes.button} aria-label="Add">
                    <AddCircleOutline />
                </IconButton>    
                <TextField
                  id="name"
                  label="Введите название "
                  className='valueInput'
                  value={this.state.name}
                  onChange={this.handleChange()}
                  margin="none"
                />
                <IconButton className={classes.button} aria-label="Search">
                    <SearchIcon />
                </IconButton>
            </div>
        )
    }
}
AddSearch.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(AddSearch);