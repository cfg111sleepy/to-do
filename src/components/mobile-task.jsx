import React, {Component} from 'react'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import Card from '@material-ui/core/Card'
import Create from '@material-ui/icons/Create'
import CheckBox from '@material-ui/icons/CheckBox'
import ExpandMore from '@material-ui/icons/ExpandMore'
import CalendarToday from '@material-ui/icons/CalendarToday'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import InputAdornment from '@material-ui/core/InputAdornment';
import ChevronRight from '@material-ui/icons/ChevronRight'



class MobileTask extends Component {
    constructor() {
        super()

        this.state = {
            isOpen: false
        }
    }
    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    getBody() {
        if (this.state.isOpen) return this.props.task
        else return null
    }
    render() {
        return (
            <div className="task-mobile" >
                <Card>
                    <div className="task-mobile-title" >
                    <Input
                      id="text"
                      type='text'
                      multiline
                      value={this.props.title}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton>
                            <Create /> 
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    </div>
                    <div className="task-mobile-body">
                        <IconButton  aria-label="CheckBox">
                            <CheckBox  color="primary"/>
                        </IconButton>    
                        <Typography variant="body1" gutterBottom color="primary" style={{marginTop: '2%'}}>
                            #{this.props.id}
                        </Typography>
                        <IconButton  aria-label="Calendar">
                            <CalendarToday />
                        </IconButton>
                        <Typography variant="body1" style={{marginLeft: '-4%'}} >
                            26 марта
                        </Typography>
                        <Avatar style={{width: 25,height: 25}} >IP</Avatar>
                        <Typography variant="body1">
                            done
                        </Typography>
                        <IconButton  aria-label="ExpandMore" onClick={this.handleClick}>
                            { this.state.isOpen ? <ExpandMore  /> : <ChevronRight /> }
                        </IconButton>
                    </div>
                    {this.getBody()}
                </Card>
            </div>
        )
    }
}

export default MobileTask