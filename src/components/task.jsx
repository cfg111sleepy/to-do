import React, {Component} from 'react'
import MobileTask from './mobile-task'
import DesktopTask from './desktop-task'
import { Container, Draggable } from 'react-smooth-dnd';
import First from './nesting/first'
import DragHandle from '@material-ui/icons/DragHandle'
import applyDrag from './functions/applyDrag'



class Task extends Component {
    constructor() {
        super()
        this.state = {
          items: [
            {   
                id: 1,
                title: "Her old collecting she considered discovered. So at parties he warrant oh staying."
            },
            {
                id: 2,
                title: "Her old collecting she considered discovered."
            },
            {
                id: 3,
                title: "So at parties he warrant oh staying."
            }
          ],
            
        }
    }

    

    render() {

        return (
          <div>
            <div className="simple-page">
              <Container groupName="1" dragHandleSelector=".column-drag-handle" getChildPayload={i => this.state.items[i]} onDrop={e => this.setState({ items: applyDrag(this.state.items, e) })}>
                {this.state.items.map(p => {
                  return (
                    <Draggable key={p.id}>
                      <div>
                        <DragHandle className="column-drag-handle" style={{float:'left'}}/>
                        <MobileTask title={p.title} id={p.id} task={<First margin="3%" />} />
                        <DesktopTask title={p.title} id={p.id} task={<First margin="3%" />} />
                      </div>
                    </Draggable>
                  );
                })}
              </Container>
            </div>
          </div>
        )
    }
}

export default Task