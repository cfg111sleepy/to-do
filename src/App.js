import React, { Component } from 'react';
import logo from './logo.svg';
import AddSearch from './components/add-search'
import TaskList from './components/task-list'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Task from './components/task'
import Collapse from '@material-ui/core/Collapse';
import './App.css'


class App extends Component {
  constructor() {
    super()
    this.state = {
      isOpen: false
    }
  }
  handleOpen = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }
  getTask() {
    if(this.state.isOpen)
        return <Task />
    else
        return null
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className='mobile-header'>
          <AppBar position="static" color="default">
            <Toolbar variant="dense">
              <IconButton className='menuButton' color="inherit" aria-label="Menu" onClick={this.handleOpen} >
                <MenuIcon />
              </IconButton>
              <Typography variant="title" color="inherit">
                Задачи команды
              </Typography>
            </Toolbar>
          </AppBar>
        </div>

        <div className="main">   
          <AddSearch/>
          <TaskList />
        </div>
        <div>
          <Collapse in={this.state.isOpen} timeout="auto" unmountOnExit>  
            {this.getTask()}
          </Collapse>
        </div>
      </div>
    );
  }
}

export default App;
